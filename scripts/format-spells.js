const fs = require('fs');
const _ = require('lodash');
const TOML = require('@iarna/toml');

fs.readFile('./spells.json', 'utf8', (err, data) => {
	const obj = JSON.parse(data);

	const spells = _.fromPairs(
		Object.entries(obj).map(entry => [
			entry[0],
			_.chunk(entry[1], 2).map(entry =>
				Object.assign(
					{},
					{
						left: entry[0],
						right: entry[1] || undefined,
					}
				)
			),
		])
	);

	console.log(
		TOML.stringify({
			extra: {
				spells,
			},
		})
	);
});
